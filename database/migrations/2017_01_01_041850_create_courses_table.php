<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quarter_id')->unsigned();
            $table->string('title',100)->nullable();
            $table->text('description')->nullable();
            $table->text('objective')->nullable();
            $table->text('outline')->nullable();
            $table->text('used_tools')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('duration');
            
            $table->timestamps();

            $table->foreign('quarter_id')->references('id')->on('quarters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExist('courses');
    }
}
