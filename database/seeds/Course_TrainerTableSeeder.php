<?php

use Illuminate\Database\Seeder;

class Course_TrainerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1,5) as $index)
        {
            DB::table('course_trainer')->insert([
                'course_id' => rand(1,5),
                'trainer_id' =>rand(1,5)
            ]);
        }
    }
}
