<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/quarters', function () {

    $quarter=\App\Quarter::find(3)->course;

    return $quarter;

});
Route::get('/trainer', function () {

    $trainer=\App\Trainer::find(4)->course;


    return $trainer;


});
Route::get('/courses', function () {

    $trainer=\App\Trainer::find(4)->course;


    return $trainer;


});


